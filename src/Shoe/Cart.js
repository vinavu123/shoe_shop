import React, { Component } from 'react'

export default class 
 extends Component {
    
  render() {
  let  {handleXoa,cart,handleCount} = this.props
    return (
      <div>
  <table class="table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Number</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
    </thead>
   { cart.map((item,index)=>{
    return(
    <tbody>
        <tr key={index}>
           
            <td>{item.name}</td>
            <td>{item.price*item.soLuong}</td>
            <td><button className='btn m-2' onClick={()=>{handleCount(item.id,-1)}}>-</button >{item.soLuong}<button className='btn m-2' onClick={()=>{handleCount(item.id,1)}}>+</button></td>
            <td>{item.description}</td>
            <td><button className='btn' onClick={()=>{handleXoa(item.id)}}>Xoa</button></td>
        </tr>
        
    </tbody>)
    })}
  </table>

</div>

    )
  }
}
