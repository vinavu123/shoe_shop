import React, { Component } from 'react'

export default class Item extends Component {
  render() {
    let {item,handleDetail,handleAdd}=this.props
    
    return (
      <div className='col-3'><div className="card text-left m-3 bg-light">
      <img className="card-img-top" src={item.image} alt />
      <div className="card-body">
        <h4 className="card-title">{item.name}</h4>
        <p className="card-text">{item.price}$</p>
        <div className="d-flex justify-content-between">
<button className='btn btn-primary' onClick={()=>{handleDetail(item)}}>Chi Tiet</button>
<button className='btn btn-success' onClick={()=>{handleAdd(item)}}>Cart</button>

        </div>
      </div>
    </div>
    </div>
    )
  }
}
