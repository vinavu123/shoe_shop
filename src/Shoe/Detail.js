import React, { Component } from 'react'

export default class  extends Component {
    
  render() {
    let {name,price,description,quantity} = this.props.item 
    return (
      <div>
<table className="table">
  <thead>
    <tr>
      <th>Name</th>
      <th>Price</th>
      <th>Description</th>
      <th>Stock</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>{name}</td>
      <td>{price}$</td>
      <td>{description}</td>
      <td>{quantity}</td>
    </tr>
   
  </tbody>
</table>


      </div>
    )
  }
}
