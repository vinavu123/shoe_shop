import React, { Component } from "react";
import List from "./List";
import { data } from "./Data";
import Detail from "./Detail";
import Cart from "./Cart";
export default class ShoeShop extends Component {
  state = {
    shoeArr: data,
    shoeDetail: data[0],
    card: [],
  };
  handleDetail = (shoe) => {
    this.setState({ shoeDetail: shoe });
  };
  handleAdd = (shoe) => {
    let cloneCart = [...this.state.card];
    let index = cloneCart.findIndex((item) => item.id == shoe.id);
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } 
    else {
      cloneCart[index].soLuong += 1;
    }
    this.setState({
      card: cloneCart,
    });
  };

  handleCount = (idShoe, option) => {
    let cloneCart = [...this.state.card];
    let index = cloneCart.findIndex((item) => item.id == idShoe);
    cloneCart[index].soLuong += option;
    if (cloneCart[index].soLuong == 0) {
      cloneCart.splice(index, 1);
    }
    this.setState({
      card: cloneCart,
    });
  };
  handleXoa = (idShoe)=>{

  let cloneCartNew = this.state.card.filter((item) => item.id !== idShoe)
  
  this.setState({card:cloneCartNew})
  }

  render() {
    return (
      <div>
        <h1 className="text-align-center text-success">Shoe Shop</h1>
        <Cart cart ={this.state.card} handleCount={this.handleCount} handleXoa = {this.handleXoa}></Cart>
        <List
          list={this.state.shoeArr}
          handleDetail={this.handleDetail}
          handleAdd={this.handleAdd}
        ></List>
        <Detail item={this.state.shoeDetail}></Detail>
      </div>
    );
  }
}
